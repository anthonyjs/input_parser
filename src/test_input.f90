PROGRAM test_input

USE input_parser
IMPLICIT NONE
LOGICAL :: eof
CHARACTER(LEN=16) :: w
CHARACTER(len=8) :: lunit="bohr", Tunit="K"
!  Internal units are bohr and kelvin
DOUBLE PRECISION :: T1, T2, DT, R1, R2, DR, lfact=1d0, Tzero=0d0, &
    value(10)
INTEGER :: i, int(10)

call input_options(echo_lines=.true.,error_flag=1)
do
  call read_line(eof)
  if (eof) exit
  call readu(w)
  select case(w)
  case("STOP","FINISH","END")
    exit
  case("")
    print "(1x)"    !  Echo blank lines
  case("UNITS")
    !  Specify external units
    do while (item < nitems)
      call reada(w)
      select case(upcase(w))
      case("CELSIUS")
        Tzero = 273.15d0
        print "(a)", "Temperatures now in Celsius externally"
        Tunit = "C"
      case("K","KELVIN")
        Tzero = 0d0
        print "(3a)", "Temperatures now in Kelvin externally"
        Tunit = "K"
      case("A","ANGSTROM")
        lfact = 0.52917725
        lunit = "angstrom"
        print "(3a)", "Distances now in ",trim(lunit)," externally"
      case("NM")
        lfact = 0.052917725
        lunit = "nm"
        print "(3a)", "Distances now in ",trim(lunit)," externally"
      case("AU","BOHR")
        lfact = 1d0
        lunit = "bohr"
        print "(3a)", "Distances now in ",trim(lunit)," externally"
      case default
        call report ("Unrecognized unit name "//trim(w),.true.)
      end select
    end do
  case("TEMPERATURES","TEMPERATURE","TEMPS")
    do while (item < nitems)
      call reada(w)
      select case(upcase(w))
      case("FROM")
        call readf(T1)
        T1 = T1+Tzero    !  Convert to Kelvin
      case("TO")
        call readf(T2)
        T2 = T2+Tzero
      case("STEP","BY")
        call readf(dT)
      case default
        call reread(-1)
        call readf(T1)
        T1 = T1+Tzero
      end select
    end do
  case("RANGE")
    do while (item < nitems)
      call reada(w)
      select case(upcase(w))
      case("FROM")
        call readf(R1,lfact)
      case("TO")
        call readf(R2,lfact)
      case("STEP","BY")
        call readf(dR,lfact)
      case default
        call reread(-1)
        call readf(R1,lfact)
      end select
    end do
  case("SHOW")
    do while (item < nitems)
      call reada(w)
      select case(upcase(w))
      case("DISTANCE","DISTANCES")
        print "(a,f8.3,a,f8.3,a,f8.3,1x,a)",                           &
            "Distances from ", R1*lfact, " to ", R2*lfact,          &
            " in steps of ", dR*lfact, trim(lunit)
      case("TEMPERATURES","TEMPS")
        print "(a,f8.3,a,f8.3,a,f8.3,1x,a)",                           &
        "Temperatures from ", T1-Tzero, " to ", T2-Tzero, " in steps of ", dT, trim(Tunit)
        
      end select
    end do
  case ("ERRORS")
    call readu(w)
    select case(w)
    case("HARD")
      call input_options(error_flag=0)
    case("ZERO")
      call input_options(error_flag=1)
    case("SOFT")
      call input_options(error_flag=2)
    end select
  case ("MISCELLANEOUS","MISC","REAL")
    i = 0
    do while (item < nitems)
      i = i+1
      call readf(value(i))
    end do
    print "(A,10F12.6)", "Values are ", value(1:i)
  case ("INTEGER")
    i = 0
    do while (item < nitems)
      i = i+1
      call readi(int(i))
    end do
    print "(A,10I6)", "Values are ", int(1:i)
  case default
    call report ("Principal keyword "//trim(w)//" not recognized",.true.)
  end select
end do

END program test_input
