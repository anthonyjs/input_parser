input_parser.F90 --- a Fortran90 module for parsing text input


The main routine in this package reads a line from an input file
(possibly a concatenation of several data lines) and splits it into
items. The items may be read and processed, possibly re-read, with
later items depending on the nature of earlier ones. It provides a very
flexible and powerful way of controlling the behaviour of a Fortran
program. Full details are provided in the doc/doc.pdf file.

To obtain the package, run the following command in a suitable
directory:
  git clone https://gitlab.com/anthonyjs/input_parser.git
This will construct a copy of the program files in the subdirectory
input_parser.

The input parser has been used extensively in my research group and
elsewhere for many years, but errors may still remain. Please email me
at ajs1@cam.ac.uk if you think you have found any.

This package is made available under the terms of the Gnu Public
Licence (http://www.gnu.org/copyleft/gpl.html). A copy of the licence
is included in the doc directory.



Anthony Stone

15 November 2018

