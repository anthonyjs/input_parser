
#  Specify the compiler that you want to use. 
FC   := gfortran
DEFS := 

#  For the Nag f95 compiler you need
#  FC   := nagfor
#  DEFS := -DNAGF95

test:  src/test_input test_data force
	src/test_input < test_data > test.out

src/test_input: src/input_parser.F90 src/test_input.f90
	cd src; ${FC} -c ${DEFS} input_parser.F90
	cd src; ${FC} -c test_input.f90
	cd src; ${FC} test_input.o input_parser.o -o test_input

force:
